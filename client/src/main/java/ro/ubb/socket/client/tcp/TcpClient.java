package ro.ubb.socket.client.tcp;

import domain.Exceptions.RepositoryException;
import ro.ubb.socket.common.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TcpClient {
    public Message sendAndReceive(Message request) {
        try (Socket socket = new Socket(Message.HOST, Message.PORT);
             InputStream is = socket.getInputStream();
             OutputStream os = socket.getOutputStream()
        ) {
            request.writeTo(os);

            Message response = new Message();
            response.readFrom(is);

            return response;
        } catch (IOException | ClassNotFoundException e) {
            throw new RepositoryException("error connection to server " + e.getMessage(), e);
        }
    }
}
