package ro.ubb.socket.client;

import ro.ubb.socket.client.service.BookServiceClient;
import ro.ubb.socket.client.service.ClientServiceClient;
import ro.ubb.socket.client.service.PurchasesServiceClient;
import ro.ubb.socket.client.service.Reports;
import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.client.ui.Console;
import ro.ubb.socket.common.ServiceInterface.ReportsInterface;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientApp {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());


        TcpClient tcpClient = new TcpClient();
        BookServiceClient bookService = new BookServiceClient(executorService, tcpClient);
        ClientServiceClient clientService = new ClientServiceClient(executorService, tcpClient);
        PurchasesServiceClient purchaseService = new PurchasesServiceClient(executorService, tcpClient);
        ReportsInterface reports = new Reports(executorService, tcpClient);
        Console console = new Console(clientService,bookService,purchaseService,reports);
        console.runConsole();

        executorService.shutdown();

        System.out.println("bye client");
    }
}
