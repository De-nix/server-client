package ro.ubb.socket.client.service;

import domain.Entities.Book;
import domain.Entities.Client;
import domain.Entities.Purchase;
import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.ServiceInterface.ReportsInterface;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class Reports implements ReportsInterface {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public Reports(ExecutorService executorService, TcpClient tcpClient) {
        this.tcpClient = tcpClient;
        this.executorService = executorService;
    }


    /**
     * @param no - the given minimum number of books that a client should have
     * @return - the set of clients that have more than x books
     */
    public CompletableFuture<Set<Client>> filterClientsWithMoreThanXBooks(int no) {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.FILTER_CLIENTS_WITH_MORE_BOOKS,no);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Client>) response.getBody();
        }, executorService);
    }


    /**
     *
     * @param clientId - the id of the client
     * @return - the sum spent by the client on bokos
     */

    /**
     * @return sorted list of the clients by the total amount of spent money
     */
    public CompletableFuture<List<Client>> sortClientsByMoneySpent() {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.SORT_CLIENTS_BY_MONEY, null);
            Message response = tcpClient.sendAndReceive(request);
            return (List<Client>) response.getBody();
        }, executorService);
    }


    /**
     * Returns all Books whose name contain the given string.
     *
     * @param s - the given string
     * @return - set of books that contains the given string
     */
    public CompletableFuture<Set<Book>> filterBooksByTitles(String s) {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.FILTER_BOOKS_BY_TITLE, s);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Book>) response.getBody();
        }, executorService);
    }

    /**
     * @param s - the given price
     * @return - a set of books that have the price greater than s
     */

    public CompletableFuture<Set<Book>> removeBookIfPriceLessThenS(int s) {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.REMOVE_BOOK_IF_PRICE_LESS_THAN_S, s);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Book>) response.getBody();
        }, executorService);
    }

    /**
     * @param s - a given string
     * @return - the set of clients whose names contain that string
     */

    public CompletableFuture<Set<Client>> filterClientsByName(String s) {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.FILTER_CLIENTS_BY_NAME, s);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Client>) response.getBody();
        }, executorService);
    }


    public CompletableFuture<Set<Purchase>> filterPurchasesByDateBefore(Date date) {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.FILTER_PURCHASES_BY_DATE_BEFORE, date);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Purchase>) response.getBody();
        }, executorService);
    }


    public CompletableFuture<Set<Purchase>> getAllPurchaseFromClientX(Long idClient) {

        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.FILTER_PURCHASES_FROM_CLIENT_X, idClient);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Purchase>) response.getBody();
        }, executorService);
    }

    public CompletableFuture<Set<Purchase>> getAllPurchaseofBookX(Long idBook) {
        return CompletableFuture.supplyAsync(() ->
        {
            Message request = new Message(ReportsInterface.FILTER_PURCHASES_FROM_book_X, idBook);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Purchase>) response.getBody();
        }, executorService);

    }
}