package ro.ubb.socket.client.service;
import domain.Entities.Client;
import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.common.ServiceInterface.clientServiceInterface;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.CompletableFuture;

public class ClientServiceClient implements clientServiceInterface{
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public ClientServiceClient(ExecutorService executorService, TcpClient tcpClient) {
        this.tcpClient = tcpClient;
        this.executorService = executorService;
    }

    public CompletableFuture<String> addClient(Client client) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(clientServiceInterface.ADD_CLIENT, client);
            Message response = tcpClient.sendAndReceive(request);
            return (String)response.getBody();
        },executorService);

    }

    /**
     * @param id - the client that will be deleted
     */
    public CompletableFuture<String> deleteClient(Long id) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(clientServiceInterface.DELETE_CLIENT,id);
            Message response = tcpClient.sendAndReceive(request);
            return (String)response.getBody();
        },executorService);

    }
    /**
     * @param client - the updated client with the same id as the old one
     */
    public CompletableFuture<String> updateClient(Client client) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(clientServiceInterface.UPDATE_CLIENT, client);
            Message response = tcpClient.sendAndReceive(request);
            return (String)response.getBody();
        },executorService);

    }

    /**
     * @param id - the id of a client
     */
    public CompletableFuture<Client> getClient(Long id) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(clientServiceInterface.GET_CLIENT, id);
            Message response = tcpClient.sendAndReceive(request);
            return (Client) response.getBody();
        },executorService);

    }


    /**
     * @return sorted the clients
     */

    public CompletableFuture<List<Client>> getSortedClients(Sort sort){
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(clientServiceInterface.GET_SORTED_CLIENTS, sort);
            Message response = tcpClient.sendAndReceive(request);
            return (List<Client>) response.getBody();
        },executorService);

    }


    @Override
    public CompletableFuture<Set<Client>> getAllClient() {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(clientServiceInterface.PRINT_ALL_CLIENT, null);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Client>) response.getBody();
        },executorService);
    }
}
