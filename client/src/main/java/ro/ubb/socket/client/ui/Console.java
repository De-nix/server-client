package ro.ubb.socket.client.ui;

import domain.Entities.Book;
import domain.Entities.Client;
import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.client.service.BookServiceClient;
import ro.ubb.socket.client.service.ClientServiceClient;
import ro.ubb.socket.client.service.PurchasesServiceClient;
import ro.ubb.socket.common.*;
import ro.ubb.socket.common.ServiceInterface.ReportsInterface;
import ro.ubb.socket.common.ServiceInterface.bookServiceInterface;
import ro.ubb.socket.common.ServiceInterface.clientServiceInterface;
import ro.ubb.socket.common.ServiceInterface.purchaseServiceInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by radu.
 */
public class Console {
    private bookServiceInterface myBookService;
    private clientServiceInterface myClientService;
    private purchaseServiceInterface myPurchaseService;
    private ReportsInterface reports;

    public Console(ClientServiceClient clientServiceClient, BookServiceClient bookServiceClient, PurchasesServiceClient purchaseServiceClient, ReportsInterface reports) {

        myBookService = bookServiceClient;
        myClientService = clientServiceClient;
        myPurchaseService = purchaseServiceClient;
        this.reports = reports;
    }

    public void runConsole() {


        boolean ok = true;

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        while (ok) {
            System.out.println("Please choose one of the above:" +
                    "\n1.Add book" +
                    "\n2.Update book" +
                    "\n3.Delete book" +
                    "\n4.Filter books by title" +
                    "\n5.Filter books by price" +
                    "\n6.Print all books" +
                    "\n7.Add client" +
                    "\n8.Delete client" +
                    "\n9.Update client" +
                    "\n10.Filter clients by name" +
                    "\n11.Filter clients by number of books" +
                    "\n12.Print all clients" +
                    "\n13.Buy a book" +
                    "\n14.Return a book" +
                    "\n15.Modify a purchase date" +
                    "\n16.Filter purchases by date : 'Before'" +
                    "\n17.Print all purchases" +
                    "\n18.Sort the clients by the money spent on books" +
                    "\n19.Sort the clients by name" +
                    "\n20.Sort the Books by price ascending and descending by name" +
                    "\n21.Sort purchases by clientID ascending and ascending by bookID" +
                    "\n0.exit\n");


            try {
                int a = Integer.parseInt(bufferRead.readLine());
                switch (a) {
                    case 0:
                        ok = false;
                        break;
                    case 1:
                        addBook();
                        break;
                    case 2:
                        updateBook();
                        break;
                    case 3:
                        deleteBook();
                        break;
                    case 4:
                        filterBooksByTitle();
                        break;
                    case 5:
                        filterBooksByPrice();
                        break;
                    case 6:
                        printAllBooks();
                        break;
                    case 7:
                        addClient();
                        break;
                    case 8:
                        deleteClient();
                        break;
                    case 9:
                        updateClient();
                        break;
                    case 10:
                        filterClientsByName();
                        break;
                    case 11:
                        filterClientsByNoBooks();
                        break;
                    case 12:
                        printAllClients();
                        break;
                    case 13:
                        buy();
                        break;
                    case 14:
                        reTurn();
                        break;
                    case 15:
                        changeDate();
                        break;
                    case 16:
                        getAllPurchasesBefore();
                        break;
                    case 17:
                        printAllPurchases();
                        break;
                    case 18:
                        sortClientsBySpentMoney();
                        break;
                    case 19:
                        sortClientsByName();
                        break;
                    case 20:
                        sortBooks();
                        break;
                    case 21:
                        sortPurchases();
                        break;

                }
            } catch (IOException ex) {
                System.err.println(ex.toString());
            } catch (ValidatorException | ParseException ex) {
                System.out.println(ex.toString());
            }


        }
    }

    private void printAllPurchases() {
        myPurchaseService.getAllPurchases().thenAccept(result -> result.forEach(System.out::println));
    }

    private void getAllPurchasesBefore() throws IOException, ParseException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the date for filtering\n");
        String lineInput;
        lineInput = bufferRead.readLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(lineInput);
        reports.filterPurchasesByDateBefore(date).thenAccept(result -> result.forEach(System.out::println));
    }

    private void changeDate() throws IOException, ValidatorException, ParseException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the client id\n");
        Long clientId = Long.valueOf(bufferRead.readLine());// ...
        System.out.println("give the book id\n");
        Long bookId = Long.valueOf(bufferRead.readLine());//
        System.out.println("give the new Date\n");
        String lineInput;
        lineInput = bufferRead.readLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(lineInput);
        myPurchaseService.changeDate(clientId, bookId, date).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });
    }

    private void reTurn() throws IOException, ValidatorException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the client id\n");
        Long clientId = Long.valueOf(bufferRead.readLine());// ...
        System.out.println("give the book id\n");
        Long bookId = Long.valueOf(bufferRead.readLine());//
        myPurchaseService.reTurn(clientId, bookId).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }

    private void buy() throws IOException, ValidatorException, ParseException {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("give the client id\n");
        Long clientId = Long.valueOf(bufferRead.readLine());// ...
        System.out.println("give the book id\n");
        Long bookId = Long.valueOf(bufferRead.readLine());//
        System.out.println("give the date of the purchase\n");
        String lineInput;
        lineInput = bufferRead.readLine();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(lineInput);
        myPurchaseService.buy(new Purchase(clientId, bookId, date)).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }

    private Long readId() {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the id of the entity:\n");
        return inn.nextLong();
    }

    private void filterBooksByTitle() {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the filter parameter\n");
        String str = inn.nextLine();
        System.out.println("Filtered Books (title containing '" + str + "'):\n");
        reports.filterBooksByTitles(str).thenAccept(result -> result.forEach(System.out::println));
    }

    private void filterBooksByPrice()  {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the price:\n");
        int price = inn.nextInt();
        reports.removeBookIfPriceLessThenS(price).thenAccept(result -> result.forEach(System.out::println));

    }

    private void printAllBooks() {

        myBookService.getAllBook().thenAccept(result -> result.forEach(System.out::println));

    }


    //common - pachet service - 3 interfete
    //BookService interface - host+port

    private void printAllClients() {
        myClientService.getAllClient().thenAccept(result -> result.forEach(System.out::println));


    }

    private void addBook() throws ValidatorException, IOException {

        Book book = readBook();
        myBookService.addBook(book).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }

    private void deleteBook() throws ValidatorException, IOException {
        Long id = readId();
        myBookService.deleteBook(id).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }

    private void updateBook() throws ValidatorException, IOException {

        Book book = readBook();
        myBookService.updateBook(book).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }


    private Book readBook() throws IOException {
        System.out.println("Read Book {id,title, author, price}\n");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Give the id of the book\n");
        Long id = Long.valueOf(bufferRead.readLine());

        System.out.println("Give the Title of the book\n");
        String title = bufferRead.readLine();
        System.out.println("Give the author of the book\n");
        String author = bufferRead.readLine();
        System.out.println("Give the price of the book\n");
        int price = Integer.parseInt(bufferRead.readLine());// ...
        //bufferRead.close();
        Book book = new Book(title, author, price);
        book.setId(id);
        return book;

    }

    private void addClient() throws ValidatorException, IOException {
        Client client = readClient();
        myClientService.addClient(client).
                thenAccept(result -> {
                    if (result.contains("ERROR")) System.err.println(result);
                    else System.out.println(result);
                });
    }

    private void deleteClient() throws ValidatorException, IOException {
        Long id = readId();
        myClientService.deleteClient(id).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }

    private void updateClient() throws ValidatorException, IOException {
        Client client = readClient();
        myClientService.updateClient(client).thenAccept(result -> {
            if (result.contains("ERROR")) System.err.println(result);
            else System.out.println(result);
        });

    }

    private Client readClient() throws IOException {
        System.out.println("Read Client {id,name}\n");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Give the id of the client\n");
        Long id = Long.valueOf(bufferRead.readLine());
        System.out.println("Give the name of the client\n");
        String name = bufferRead.readLine();
        Client client = new Client(name);
        client.setId(id);
        return client;
    }

    private void filterClientsByName() {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the filter parameter\n");
        String str = inn.nextLine();
        System.out.println("Filtered Clients (name containing '" + str + "'):\n");
        reports.filterClientsByName(str).thenAccept(result -> result.forEach(System.out::println));

    }

    private void filterClientsByNoBooks()  {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter the NO books a client must have:\n");
        int booksNO = inn.nextInt();
        System.out.println("Filtered Clients (NO books greater than '" + booksNO + "'):\n");

        reports.filterClientsWithMoreThanXBooks(booksNO).thenAccept(result -> result.forEach(System.out::println));

    }

    private void sortClientsBySpentMoney() {
        reports.sortClientsByMoneySpent().thenAccept(result -> result.forEach(System.out::println));
    }


    private void sortClientsByName()  {
        Scanner inn = new Scanner(System.in);
        System.out.println("Enter 1 for ascending sorting or 2 for descending sorting\n");
        int x = inn.nextInt();
        Sort sort;
        if (x == 1) {
            sort = new Sort("name");
        } else sort = new Sort(Sort.Dir.descending, "name");

        myClientService.getSortedClients(sort).thenAccept(result -> result.forEach(System.out::println));

    }

    private void sortBooks() {
        Sort sort = new Sort("price").add(new Sort(Sort.Dir.descending, "title"));
        myBookService.getSortedBooks(sort).thenAccept(result -> result.forEach(System.out::println));

    }

    private void sortPurchases() {
        Sort sort = new Sort("idClient", "idBook");
        myPurchaseService.getSortedPurchases(sort).thenAccept(result -> result.forEach(System.out::println));
    }
}


