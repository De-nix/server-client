package ro.ubb.socket.client.service;
import domain.Entities.Book;
import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.common.ServiceInterface.bookServiceInterface;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class BookServiceClient implements bookServiceInterface {
    private ExecutorService executorService;
    private TcpClient tcpClient;

    public BookServiceClient(ExecutorService executorService, TcpClient tcpClient) {
        this.tcpClient = tcpClient;
        this.executorService = executorService;
    }
    /**
     * @param book - the book that will be added
     */
    public CompletableFuture<String> addBook(Book book)  {
        return CompletableFuture.supplyAsync((() -> {
            Message request = new Message(bookServiceInterface.ADD_BOOK, book);
            Message response = tcpClient.sendAndReceive(request);
            return (String)response.getBody();
        }),executorService);
    }

    /**
     * @param id - the id of the book that will be deleted
     */
    public CompletableFuture<String> deleteBook(Long id){
        return CompletableFuture.supplyAsync((() -> {
            Message request = new Message(bookServiceInterface.DELETE_BOOK, id);
            Message response = tcpClient.sendAndReceive(request);
            return (String)response.getBody();
        }),executorService);
    }

    /**
     * @param book - the updated book with the same id as the old one
     */
    public CompletableFuture<String> updateBook(Book book) {
        return CompletableFuture.supplyAsync((() -> {
            Message request = new Message(bookServiceInterface.UPDATE_BOOK, book);
            Message response = tcpClient.sendAndReceive(request);
            return (String)response.getBody();
        }),executorService);
    }

    /**
     * @param id - id of the book
     * @return - the book with id = 'id'
     */
    public CompletableFuture<Book> getBook(Long id)  {
        return CompletableFuture.supplyAsync((() -> {
            Message request = new Message(bookServiceInterface.GET_BOOK, id);
            Message response = tcpClient.sendAndReceive(request);
            return (Book)response.getBody();
        }),executorService);
    }



    @Override
    public CompletableFuture<Set<Book>> getAllBook() {
        return CompletableFuture.supplyAsync((() -> {
            Message request = new Message(bookServiceInterface.PRINT_ALL_BOOK,null);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Book>)response.getBody();
        }),executorService);
    }


    public CompletableFuture<List<Book>> getSortedBooks(Sort sort){
        return CompletableFuture.supplyAsync((() -> {
            Message request = new Message(bookServiceInterface.GET_SORTED_BOOKS,sort);
            Message response = tcpClient.sendAndReceive(request);
            return (List<Book>)response.getBody();
        }),executorService);
    }
}
