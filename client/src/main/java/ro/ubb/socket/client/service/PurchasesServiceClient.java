package ro.ubb.socket.client.service;

import domain.Entities.Purchase;
import javafx.util.Pair;

import ro.ubb.socket.client.tcp.TcpClient;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.common.ServiceInterface.purchaseServiceInterface;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class PurchasesServiceClient implements purchaseServiceInterface {

    private ExecutorService executorService;
    private TcpClient tcpClient;

    public PurchasesServiceClient(ExecutorService executorService, TcpClient tcpClient) {
        this.tcpClient = tcpClient;
        this.executorService = executorService;
    }


    public CompletableFuture<String> buy(Purchase p) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(purchaseServiceInterface.BUY_BOOK, p);
            Message response = tcpClient.sendAndReceive(request);
            return (String) response.getBody();
        }, executorService);

    }

    public CompletableFuture<String> reTurn(Long clientId, Long bookId) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(purchaseServiceInterface.RETURN_BOOK, new Pair<>(clientId, bookId));
            Message response = tcpClient.sendAndReceive(request);
            return (String) response.getBody();
        }, executorService);
    }

    public CompletableFuture<String> changeDate(Long clientId, Long bookId, Date date) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(purchaseServiceInterface.CHANGE_DATE, new Pair<>(clientId, new Pair<>(bookId, date)));
            Message response = tcpClient.sendAndReceive(request);
            return (String) response.getBody();
        }, executorService);
    }

    public CompletableFuture<Purchase> getPurchase(Long clientId, Long bookId) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(purchaseServiceInterface.GET_PURCHASE, new Pair<>(clientId, bookId));
            Message response = tcpClient.sendAndReceive(request);
            return (Purchase) response.getBody();
        }, executorService);
    }

    public CompletableFuture<Set<Purchase>> getAllPurchases() {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(purchaseServiceInterface.PRINT_ALL_PURCHASE, null);
            Message response = tcpClient.sendAndReceive(request);
            return (Set<Purchase>) response.getBody();
        }, executorService);
    }

    public CompletableFuture<List<Purchase>> getSortedPurchases(Sort sort) {
        return CompletableFuture.supplyAsync(() -> {
            Message request = new Message(purchaseServiceInterface.GET_SORTED_PURCHASES, sort);
            Message response = tcpClient.sendAndReceive(request);
            return (List<Purchase>) response.getBody();
        }, executorService);
    }
}
    

