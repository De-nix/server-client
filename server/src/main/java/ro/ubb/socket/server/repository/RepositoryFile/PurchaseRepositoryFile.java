package ro.ubb.socket.server.repository.RepositoryFile;

import domain.Entities.Purchase;
import domain.Exceptions.RepositoryException;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.server.repository.InMemoryRepository;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class PurchaseRepositoryFile extends InMemoryRepository<Long, Purchase> {
    private String fileName;

    public PurchaseRepositoryFile(Validator<Purchase> validator, Validator<Long> keyValidator, String fileName) throws IOException {
        super(validator, keyValidator);
        this.fileName = fileName;
        loadData();
    }

    private void loadData() throws IOException {
        Path path = Paths.get(fileName);
        System.out.println(path.toAbsolutePath());

        Files.lines(path).forEach(line -> {
            List<String> items = Arrays.asList(line.split(","));

            Long idClient = Long.valueOf(items.get(0));
            Long idBook = Long.valueOf(items.get(1));
            Date date = null;

            try {
                date = new SimpleDateFormat("dd/MM/yyyy").parse(items.get(2));
            } catch (ParseException ign) {
                throw new RepositoryException("Invalid data load!\n");
            }
            Purchase purchase = new Purchase(idClient,idBook,date);
            try {
                super.save(purchase);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }

        });

    }
    @Override
    public Optional<Purchase> save(Purchase entity) throws ValidatorException {
        Optional<Purchase> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Purchase> update(Purchase entity) throws ValidatorException, IOException {
        Optional<Purchase> optional = super.update(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveAllToFile();
        return Optional.empty();
    }

    @Override
    public Optional<Purchase> delete(Long id) throws ValidatorException, IOException {
        Optional<Purchase> optional = super.delete(id);
        if (!optional.isPresent()) {
            return optional;
        }
        saveAllToFile();
        return Optional.empty();
    }

    private void saveToFile(Purchase entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            bufferedWriter.write(entity.getIdClient() + "," +entity.getIdBook() + "," + formatter.format(entity.getDate()));
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void saveAllToFile() throws IOException {
        Path path = Paths.get(fileName);


        Set<Purchase> purchases = new HashSet<>();
        this.findAll().forEach(purchases::add);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try(BufferedWriter bufferedWriter = Files.newBufferedWriter(path)){
        purchases.forEach(entity->{
            try {
                bufferedWriter.write(entity.getIdClient() + "," +entity.getIdBook() + "," + formatter.format(entity.getDate()));
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    }
}
