package ro.ubb.socket.server.repository.RepositoryFile;

import domain.Entities.Book;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.server.repository.InMemoryRepository;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

//
public class BookRepositoryFile extends InMemoryRepository<Long, Book> {
    private final String fileName;

    public BookRepositoryFile(Validator<Book> validator, Validator<Long> keyValidator, String fileName) {
        super(validator,keyValidator);
        this.fileName = fileName;

        loadData();
    }

    private void loadData() {
        Path path = Paths.get(fileName);
        System.out.println(path.toAbsolutePath());

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long id = Long.valueOf(items.get(0));
                String title = items.get(1);
                String author = items.get((2));
                int price = Integer.parseInt(items.get(3));
                Book book = new Book(title,author,price);
                book.setId(id);

                try {
                    super.save(book);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Book> save(Book entity) throws ValidatorException {
        Optional<Book> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Book> update(Book entity) throws ValidatorException, IOException {
        Optional<Book> optional = super.update(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveAllToFile();
        return Optional.empty();
    }

    @Override
    public Optional<Book> delete(Long id) throws ValidatorException, IOException {
        Optional<Book> optional = super.delete(id);
        if (!optional.isPresent()) {
            return optional;
        }
        saveAllToFile();
        return Optional.empty();
    }

    private void saveToFile(Book entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getTitle() + "," + entity.getAuthor() + "," + entity.getPrice());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void saveAllToFile() throws IOException {
        Path path = Paths.get(fileName);

        BufferedWriter bufferedWriter = Files.newBufferedWriter(path) ;
        this.findAll().forEach(entity->
        {
            try {
                bufferedWriter.write(
                        entity.getId() + "," + entity.getTitle() + "," + entity.getAuthor() + "," + entity.getPrice());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
