package ro.ubb.socket.server.service.Service;

import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.SortingRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PurchaseService{


    private SortingRepository<Long, Purchase> repository;

    public PurchaseService(SortingRepository<Long, Purchase> repository) {
        this.repository = repository;
    }

    public synchronized void addPurchase(Purchase purchase) throws ValidatorException {
        if(findPurchase(purchase.getIdClient(),purchase.getIdBook()) == -1L)repository.save(purchase);
    }

    public synchronized void deletePurchase(Long idClient, Long idBook) throws ValidatorException, IOException {
        Long id = findPurchase(idClient,idBook);
        repository.delete(id);
    }

    public synchronized Purchase getPurchase(Long id) throws ValidatorException {
        return repository.findOne(id).orElseThrow(()->new ValidatorException("The searched book doesn't exist\n"));
    }

    public synchronized void updatePurchase(Purchase purchase) throws ValidatorException, IOException {
        repository.update(purchase);
    }

    public synchronized Set<Purchase> getAllPurchases() {
        Iterable<Purchase> purchases = repository.findAll();
        return StreamSupport.stream(purchases.spliterator(), false).collect(Collectors.toSet());
    }
    public synchronized Long findPurchase(Long idClient,Long idBook){
        List<Purchase> listP = new ArrayList<>();
        repository.findAll().forEach(listP::add);
        Optional<Purchase> optional=  listP.stream().filter(x->x.getIdClient().equals(idClient) && x.getIdBook().equals(idBook)).findFirst();
        if (optional.isPresent()) return optional.get().getId();
        return -1L;
    }

    public synchronized List<Purchase> getPurchasesSorted(Sort sort){
        return (List<Purchase>) repository.findAll(sort);
    }

}
