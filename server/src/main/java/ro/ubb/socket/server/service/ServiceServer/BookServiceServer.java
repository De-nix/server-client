package ro.ubb.socket.server.service.ServiceServer;
import domain.Entities.Book;
import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.common.ServiceInterface.bookServiceInterface;
import ro.ubb.socket.server.service.Service.BookService;
import ro.ubb.socket.server.service.Service.PurchaseService;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;


public class BookServiceServer implements bookServiceInterface {
    BookService bookService;
    PurchaseService purchaseService;
    private ExecutorService executorService;

    public BookServiceServer(BookService bookService, PurchaseService purchaseService, ExecutorService executorService) {
        this.bookService = bookService;
        this.purchaseService = purchaseService;
        this.executorService = executorService;
    }

    /**
     * @param book - the book that will be added
     */
    public synchronized CompletableFuture<String> addBook(Book book){

        return CompletableFuture.supplyAsync(() ->
        {
            try {
                bookService.getBook(book.getId());
                return "ERROR : The book already exists";
            } catch (ValidatorException ignore){
            }
            try{
                bookService.addBook(book);
                return "The book was successfully added\n";
            } catch (ValidatorException e) {
                return "ERROR : "+e.toString()+"\n";
            }
        });
    }

    /**
     * @param id - the id of the book that will be deleted
     */
    public synchronized CompletableFuture<String> deleteBook(Long id) {


        return CompletableFuture.supplyAsync(() ->
        {
            try {
                bookService.getBook(id);
                purchaseService.getAllPurchases().stream().filter(x -> x.getIdBook().equals(id)).map(Purchase::getIdClient).forEach(x -> {
                    try {
                        purchaseService.deletePurchase(x, id);
                    } catch (ValidatorException |IOException ignored) {
                    }
                });

                bookService.deleteBook(id);
                return "The book was successfully deleted\n";
            } catch (ValidatorException | IOException e) {
                return "ERROR : "+e.toString()+"\n";
            }
        });

    }

    /**
     * @param book - the updated book with the same id as the old one
     */
    public synchronized CompletableFuture<String> updateBook(Book book){
        return CompletableFuture.supplyAsync(() ->
        {
            try {
                bookService.getBook(book.getId());
                bookService.updateBook(book);
                return "The book was successfully updated\n";
            } catch (ValidatorException | IOException e) {
                return "ERROR : "+e.toString()+"\n";
            }
        });
    }

    /**
     * @param id - id of the book
     * @return - the book with id = 'id'
     */
    public synchronized CompletableFuture<Book> getBook(Long id) {
        return CompletableFuture.supplyAsync(() ->
        {
            try {
                return bookService.getBook(id);
            } catch (ValidatorException e) {
               Book b = new Book(e.toString(),"",-1);
               b.setId(-1L);
               return b;
            }
        });
    }


    @Override
    public synchronized CompletableFuture<Set<Book>> getAllBook() {
        return CompletableFuture.supplyAsync(() ->
                bookService.getAllBooks(),executorService);
    }


    public synchronized CompletableFuture<List<Book>> getSortedBooks(Sort sort) {
        return CompletableFuture.supplyAsync(() ->
                bookService.getBooksSorted(sort),executorService);
    }
}
