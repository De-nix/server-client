package ro.ubb.socket.server.service.Service;

import domain.Entities.Client;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.SortingRepository;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ClientService {
    private SortingRepository<Long, Client> repository;

    public ClientService(SortingRepository<Long, Client> repository) {
        this.repository = repository;
    }

    public synchronized void addClient(Client client) throws ValidatorException {
        repository.save(client);
    }

    public synchronized void deleteClient(Long id) throws ValidatorException, IOException {
        repository.delete(id);
    }

    public synchronized List<Client> getClientsSortedByName(Sort sort){

        return (List<Client>) repository.findAll(sort);
    }

    public synchronized Client getClient(Long id) throws ValidatorException {
        return repository.findOne(id).orElseThrow(()->new ValidatorException("The searched book doesn't exist\n"));
    }

    public synchronized void updateClient(Client client) throws ValidatorException, IOException {
        repository.update(client);
    }

    public synchronized Set<Client> getAllClients() {
        Iterable<Client> clients = repository.findAll();
        return StreamSupport.stream(clients.spliterator(), false).collect(Collectors.toSet());
    }


}
