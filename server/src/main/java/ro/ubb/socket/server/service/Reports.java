package ro.ubb.socket.server.service;

import domain.Entities.Book;
import domain.Entities.Client;
import domain.Entities.Purchase;
import ro.ubb.socket.common.ServiceInterface.ReportsInterface;
import ro.ubb.socket.server.service.Service.BookService;
import ro.ubb.socket.server.service.Service.ClientService;
import ro.ubb.socket.server.service.Service.PurchaseService;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

public class Reports implements ReportsInterface {

    BookService bookService;
    ClientService clientService;
    PurchaseService purchaseService;
    private ExecutorService executorService;

    public Reports(ClientService clientService, BookService bookService, PurchaseService purchaseService, ExecutorService executorService) {
        this.bookService = bookService;
        this.clientService = clientService;
        this.purchaseService = purchaseService;
        this.executorService = executorService;
    }

    /**
     * @param no - the given minimum number of books that a client should have
     * @return - the set of clients that have more than x books
     */
    public synchronized CompletableFuture<Set<Client>> filterClientsWithMoreThanXBooks(int no) {
        return CompletableFuture.supplyAsync(() ->
                clientService.getAllClients().stream().
                        filter(x ->
                                no < purchaseService.getAllPurchases().stream().
                                        filter(y -> y.getIdClient().equals(x.getId())).count()).
                        collect(Collectors.toSet()), executorService);
    }

    /**
     * @param clientId - the id of the client
     * @return - the sum spent by the client on bokos
     */
    public synchronized Integer spentMoney(Long clientId) {

        Set<Long> bookIds = purchaseService.getAllPurchases().stream().filter(x -> x.getIdClient().
                equals(clientId)).map(Purchase::getIdBook).collect(Collectors.toSet());
        return bookService.getAllBooks().stream().filter(x -> bookIds.contains(x.getId())).
                map(Book::getPrice).reduce(0, Integer::sum);
    }

    /**
     * @return sorted list of the clients by the total amount of spent money
     */
    public synchronized CompletableFuture<List<Client>> sortClientsByMoneySpent() {

        return CompletableFuture.supplyAsync(() ->
        {
            List<Client> clients = new ArrayList<>(clientService.getAllClients());
            clients.sort(Comparator.comparing(c -> spentMoney(c.getId())));
            return clients;
        }, executorService);

    }


    /**
     * Returns all Books whose name contain the given string.
     *
     * @param s - the given string
     * @return - set of books that contains the given string
     */
    public synchronized CompletableFuture<Set<Book>> filterBooksByTitles(String s) {
        return CompletableFuture.supplyAsync(() ->
        {
            Set<Book> filteredBooks = bookService.getAllBooks();
            filteredBooks.removeIf(Book -> !Book.getTitle().contains(s));
            return filteredBooks;
        }, executorService);
    }

    /**
     * @param s - the given price
     * @return - a set of books that have the price greater than s
     */

    public synchronized CompletableFuture<Set<Book>> removeBookIfPriceLessThenS(int s) {

        return CompletableFuture.supplyAsync(() ->
        {
            Set<Book> filteredBooks = bookService.getAllBooks();
            filteredBooks.removeIf(book -> book.getPrice() <= s);
            return filteredBooks;
        },executorService);
    }

    /**
     * @param s - a given string
     * @return - the set of clients whose names contain that string
     */

    public synchronized CompletableFuture<Set<Client>> filterClientsByName(String s) {

        return CompletableFuture.supplyAsync(() ->
        {
            Set<Client> filteredClients = clientService.getAllClients();
            filteredClients.removeIf(client -> !client.getName().contains(s));
            return filteredClients;
        },executorService);
    }


    public synchronized CompletableFuture<Set<Purchase>> filterPurchasesByDateBefore(Date date) {

        return CompletableFuture.supplyAsync(() ->
                purchaseService.getAllPurchases().stream().filter(x -> x.getDate().before(date)).
                        collect(Collectors.toSet()),executorService);
    }


    public synchronized CompletableFuture<Set<Purchase>> getAllPurchaseFromClientX(Long idClient) {

        return CompletableFuture.supplyAsync(() ->
                purchaseService.getAllPurchases().stream().filter(x -> x.getIdClient().equals(idClient)).
                        collect(Collectors.toSet()),executorService);
    }

    public synchronized CompletableFuture<Set<Purchase>> getAllPurchaseofBookX(Long idBook) {
        return CompletableFuture.supplyAsync(() ->
                purchaseService.getAllPurchases().stream().filter(x -> x.getIdBook().equals(idBook)).
                        collect(Collectors.toSet()),executorService);
    }

}