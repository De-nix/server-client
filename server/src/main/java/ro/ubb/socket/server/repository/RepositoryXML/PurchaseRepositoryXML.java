package ro.ubb.socket.server.repository.RepositoryXML;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.server.repository.InMemoryRepository;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class PurchaseRepositoryXML extends InMemoryRepository<Long, Purchase> {
    private String filename;

    public PurchaseRepositoryXML(Validator<Purchase> validator, Validator<Long> keyValidator, String file) {
        super(validator, keyValidator);
        this.filename = file;
        loadData();
    }

    private void loadData(){
        File file = new File(filename);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("Purchase");
            for(int i=0;i< nodeList.getLength();i++){
                Node node = nodeList.item(i);
                Element e  = (Element) node;
                long idClient = Long.parseLong(String.valueOf(e.getAttributeNode("IdClient").getValue()));
                long idBook = Long.parseLong(String.valueOf(e.getAttributeNode("IdBook").getValue()));
                Date date = new SimpleDateFormat("dd/MM/yyyy").parse(String.valueOf(e.getAttributeNode("Date").getValue()));
                Purchase newp = new Purchase(idClient,idBook,date);
                super.save(newp);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Purchase> save(Purchase entity) throws ValidatorException {
        Optional<Purchase> opt = super.save(entity);
        writeData();
        return opt;
    }

    @Override
    public Optional<Purchase> delete(Long aLong) throws ValidatorException, IOException {
        Optional<Purchase> opt = super.delete(aLong);
        writeData();
        return opt;
    }

    @Override
    public Optional<Purchase> update(Purchase entity) throws ValidatorException, IOException {
        Optional<Purchase> opt = super.update(entity);
        writeData();
        return opt;
    }

    private void writeData(){
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Purchases");
            doc.appendChild(rootElement);

            Iterable<Purchase> allPurchases = super.findAll();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            StreamSupport.stream(allPurchases.spliterator(),false).forEach(p->{
                Element purchase = doc.createElement("Purchase");
                rootElement.appendChild(purchase);
                purchase.setAttribute("IdClient",String.valueOf(p.getIdClient()));
                purchase.setAttribute("IdBook",String.valueOf(p.getIdBook()));
                purchase.setAttribute("Date", formatter.format(p.getDate()));

            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filename));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);



        }
        catch (Exception e){
            System.out.println("write data did not work for purchase");
        }
    }
}
