package ro.ubb.socket.server.service.Service;


import domain.Entities.Book;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.SortingRepository;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class BookService {
    private SortingRepository<Long, Book> repository;

    public BookService(SortingRepository<Long, Book> repository) {
        this.repository = repository;
    }

    public synchronized void addBook(Book book) throws ValidatorException {
        repository.save(book);
    }

    public synchronized void deleteBook(Long id) throws ValidatorException, IOException {
        repository.delete(id);
    }

    public synchronized void updateBook(Book book) throws ValidatorException, IOException {
        repository.update(book);
    }
    public synchronized Book getBook(Long id) throws ValidatorException {
        return repository.findOne(id).orElseThrow(()->new ValidatorException("The searched book doesn't exist\n"));
    }

    public synchronized List<Book> getBooksSorted(Sort sort){

        return (List<Book>) repository.findAll(sort);
    }

    public synchronized Set<Book> getAllBooks() {
        Iterable<Book> Books = repository.findAll();
        return StreamSupport.stream(Books.spliterator(), false).collect(Collectors.toSet());
    }


}
