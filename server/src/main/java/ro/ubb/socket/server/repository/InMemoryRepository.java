package ro.ubb.socket.server.repository;

import domain.Entities.BaseEntity;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.common.Sort;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class InMemoryRepository<ID extends Serializable, T extends BaseEntity<ID>> implements SortingRepository<ID, T> {
    private Map<ID, T> entities;
    private Validator<T> validator;
    private Validator<ID> keyValidator;

    public InMemoryRepository(Validator<T> validator, Validator<ID> keyValidator) {
        this.validator = validator;
        this.keyValidator = keyValidator;
        entities = new HashMap<>();
    }

    @Override
    public Optional<T> findOne(ID id) throws ValidatorException {
        keyValidator.validate(id);
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public Iterable<T> findAll() {
        return new HashSet<>(entities.values());
    }

    @Override
    public Optional<T> save(T entity) throws ValidatorException {
        validator.validate(entity);
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    @Override
    public Optional<T> delete(ID id) throws ValidatorException, IOException {
        keyValidator.validate(id);
        return Optional.ofNullable(entities.remove(id));
    }

    @Override
    public Optional<T> update(T entity) throws ValidatorException, IOException {
        validator.validate(entity);
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k, v) -> entity));
    }

    @Override
    public Iterable<T> findAll(Sort sort) {
        List<T> all = new ArrayList<>(entities.values());
        //all.stream().forEach(student -> System.out.println(student));
        all.sort(sort);
        return all;
    }
}
