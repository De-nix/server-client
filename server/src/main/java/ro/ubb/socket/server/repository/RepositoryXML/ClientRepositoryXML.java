package ro.ubb.socket.server.repository.RepositoryXML;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import domain.Entities.Client;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.server.repository.InMemoryRepository;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class ClientRepositoryXML extends InMemoryRepository<Long, Client> {
    private String filename;

    public ClientRepositoryXML(Validator<Client> validator, Validator<Long> keyValidator, String file) {
        super(validator, keyValidator);
        this.filename = file;
        loadData();
    }

    private void loadData(){
        File file = new File(filename);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("Client");
            for(int i=0;i< nodeList.getLength();i++){
                Node node = nodeList.item(i);
                Element e  = (Element) node;
                long id = Long.parseLong(String.valueOf(e.getAttributeNode("Id").getValue()));
                String name = String.valueOf(e.getAttributeNode("Name").getValue());

                Client newclient = new Client(name);
                newclient.setId(id);
                super.save(newclient);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        Optional<Client> opt =  super.save(entity);
        writeData();
        return opt;
    }

    @Override
    public Optional<Client> delete(Long id) throws ValidatorException, IOException {
        Optional<Client> opt = super.delete(id);
        writeData();
        return opt;
    }

    @Override
    public Optional<Client> update(Client entity) throws ValidatorException, IOException {
        Optional<Client> opt =super.update(entity);
        writeData();
        return opt;
    }

    private void writeData(){
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Clients");
            doc.appendChild(rootElement);

            Iterable<Client> allClients = super.findAll();
            StreamSupport.stream(allClients.spliterator(),false).forEach(cl->{
                Element client = doc.createElement("Client");
                rootElement.appendChild(client);

                client.setAttribute("Id",String.valueOf(cl.getId()));
                client.setAttribute("Name",String.valueOf(cl.getName()));

            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filename));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);

        }
        catch (Exception e){
            System.out.println("write data did not work for client");
        }
    }



}
