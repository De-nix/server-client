package ro.ubb.socket.server.repository.RepositoryDB;


import domain.Entities.Client;
import domain.Exceptions.ValidatorException;
import domain.Validators.ClientValidator;
import domain.Validators.Validator;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.SortingRepository;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class ClientRepositoryDB implements SortingRepository<Long, Client> {

    private Validator<Client> clientValidator;
    private Validator<Long> keyValidator;
    private HashMap<Long, Client> entities;
    private static final String URL = "jdbc:postgresql://localhost:5432/BookStore";
    private static final String USER = "postgres";
    private static final String PASSWORD = "123";


    public ClientRepositoryDB(Validator<Client> validator, Validator<Long> keyValidator) {
        this.keyValidator = keyValidator;
        this.clientValidator = validator;
        entities = new HashMap<>();
        loadData();
    }

    private void loadData(){
        ClientValidator validator = new ClientValidator();
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "Select * from client";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet set = stmt.executeQuery();
            while (set.next()) {
                Long id = set.getLong("id");
                String name = set.getString("name");
                Client c = new Client(name);
                c.setId(id);
                validator.validate(c);
                entities.put(id, c);
            }
            con.close();
        } catch (SQLException | ValidatorException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Iterable<Client> findAll(Sort sort) {
        List<Client> all = new ArrayList<>(entities.values());
        //all.stream().forEach(student -> System.out.println(student));
        all.sort(sort);
        return all;
    }

    @Override
    public Optional<Client> findOne(Long id) {
        if (id == null){
            throw new IllegalArgumentException("null id");
        }
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public Iterable<Client> findAll() {
        Set<Client> all = entities.entrySet().stream()
                .map(entry -> entry.getValue()).collect(Collectors.toSet());
        return all;
    }

    private void saveToDb(Client entity){
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "insert into client(id, name) values(?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, entity.getId());
            stmt.setString(2, entity.getName());
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Client> save(Client entity) {
        if (entity.getId() == null){
            throw new IllegalArgumentException("null id");
        }
        if (!entities.containsKey(entity.getId())){
            saveToDb(entity);
        }
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    private void deleteFromDb(Long id){
        Connection con = null;
        try{
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "delete from client where id=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, id);
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Client> delete(Long id) {
        if (id == null){
            throw new IllegalArgumentException("null id");
        }
        if (entities.containsKey(id)){
            deleteFromDb(id);
        }
        return Optional.ofNullable(entities.remove(id));
    }

    private void updateToDb(Client entity){
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "update client set name=? where id=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, entity.getName());
            stmt.setLong(2, entity.getId());
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Client> update(Client entity) {
        if (entity == null){
            throw new IllegalArgumentException("null entity");
        }
        if (entities.containsKey(entity.getId())){
            updateToDb(entity);
        }
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k,v) -> entity));
    }
}
