package ro.ubb.socket.server.repository.RepositoryXML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import domain.Entities.Book;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.server.repository.InMemoryRepository;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class BookRepositoryXML extends InMemoryRepository<Long, Book> {
    private String filename;

    public BookRepositoryXML(Validator<Book> validator, Validator<Long> keyValidator, String file) {
        super(validator,keyValidator);
        this.filename = file;
        loadData();
    }
    private void loadData(){
        File file = new File(filename);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("Book");
            for(int i=0; i< nodeList.getLength(); i++){
                Node node = nodeList.item(i);
                Element e  = (Element) node;
                long id = Long.parseLong(String.valueOf(e.getAttributeNode("Id").getValue()));
                String title = String.valueOf(e.getAttributeNode("Title").getValue());
                String author = String.valueOf(e.getAttributeNode("Author").getValue());
                int price = Integer.parseInt(String.valueOf(e.getAttributeNode("Price").getValue()));
                Book newbook = new Book(title,author,price);
                newbook.setId(id);
                super.save(newbook);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Book> save(Book entity) throws ValidatorException {
        Optional<Book> opt = super.save(entity);
        writeData();
        return opt;

    }

    @Override
    public Optional<Book> delete(Long id) throws ValidatorException, IOException {
        Optional<Book> opt =  super.delete(id);
        writeData();
        return opt;
    }

    @Override
    public Optional<Book> update(Book entity) throws ValidatorException, IOException {
        Optional<Book> opt = super.update(entity);
        writeData();
        return opt;
    }

    private void writeData(){
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Books");
            doc.appendChild(rootElement);

            Iterable<Book> allBooks = super.findAll();
            StreamSupport.stream(allBooks.spliterator(),false).forEach(b->{
                Element book = doc.createElement("Book");
                rootElement.appendChild(book);

                book.setAttribute("Id",String.valueOf(b.getId()));
                book.setAttribute("Title",String.valueOf(b.getTitle()));
                book.setAttribute("Author",String.valueOf(b.getAuthor()));
                book.setAttribute("Price",String.valueOf(b.getPrice()));

            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filename));
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(source, result);

        }
        catch (Exception e){
            System.out.println("write data did not work for book");
        }
    }


}
