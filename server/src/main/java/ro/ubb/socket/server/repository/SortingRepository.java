package ro.ubb.socket.server.repository;

import domain.Entities.BaseEntity;
import ro.ubb.socket.common.Sort;

import java.io.Serializable;

public interface SortingRepository<ID extends Serializable, T extends BaseEntity<ID>> extends Repository<ID, T>
{

    Iterable<T> findAll(Sort sort);

    //TODO: insert sorting-related code here
}