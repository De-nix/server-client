package ro.ubb.socket.server.service.ServiceServer;

import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.common.ServiceInterface.purchaseServiceInterface;
import ro.ubb.socket.server.service.Service.BookService;
import ro.ubb.socket.server.service.Service.ClientService;
import ro.ubb.socket.server.service.Service.PurchaseService;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class PurchaseServiceServer implements purchaseServiceInterface {

    BookService bookService;
    ClientService clientService;
    PurchaseService purchaseService;
    private ExecutorService executorService;

    public PurchaseServiceServer( ClientService clientService,BookService bookService, PurchaseService purchaseService, ExecutorService executorService) {
        this.bookService = bookService;
        this.clientService = clientService;
        this.purchaseService = purchaseService;
        this.executorService = executorService;
    }

    @Override
    public synchronized CompletableFuture<Set<Purchase>> getAllPurchases() {
        return CompletableFuture.supplyAsync(() ->
                purchaseService.getAllPurchases());
    }


    public synchronized CompletableFuture<List<Purchase>> getSortedPurchases(Sort sort) {
        return CompletableFuture.supplyAsync(() ->
                purchaseService.getPurchasesSorted(sort));
    }

///cfdhdb
    public synchronized CompletableFuture<String> buy(Purchase purchase) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                clientService.getClient(purchase.getIdClient());
                bookService.getBook(purchase.getIdBook());
            } catch (ValidatorException e) {
                return "ERROR :" + e.getMessage();
            }
            try {
                if (purchaseService.findPurchase(purchase.getIdClient(), purchase.getIdBook()) == -1) {
                    purchaseService.addPurchase(new Purchase(purchase.getIdClient(),purchase.getIdBook(),purchase.getDate()));
                    return "The Purchase was successfully registered\n";
                } else return "ERROR : The Purchase already exists in the system\n";
            } catch (ValidatorException e) {
                return "ERROR :" + e.getMessage();
            }
        });

    }

    public synchronized CompletableFuture<String> reTurn(Long clientId, Long bookId) {

        executorService.submit(() -> {
            try {
                clientService.getClient(clientId);
                bookService.getBook(bookId);
                purchaseService.deletePurchase(clientId, bookId);
            } catch (ValidatorException | IOException ignore) {

            }
        });

        return null;
    }

    public synchronized CompletableFuture<String> changeDate(Long clientId, Long bookId, Date date) {

        executorService.submit(() -> {
            try {
                Long id = purchaseService.findPurchase(clientId, bookId);
                Purchase p = purchaseService.getPurchase(id);
                p.setDate(date);
                purchaseService.updatePurchase(p);
            } catch (ValidatorException | IOException ignore) {

            }
        });

        return null;
    }


    public synchronized CompletableFuture<Purchase> getPurchase(Long clientId, Long bookId) {
        return CompletableFuture.supplyAsync(() ->
        {
            Long id = purchaseService.findPurchase(clientId, bookId);
            try {
                if (id == -1L)return new Purchase(-1L,-1L,null);
                return purchaseService.getPurchase(id);
            } catch (ValidatorException e) {
                return new Purchase(-1L,-1L,null);
            }
        });
    }

}

