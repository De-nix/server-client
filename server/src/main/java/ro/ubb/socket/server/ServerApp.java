package ro.ubb.socket.server;

import domain.Entities.Book;
import domain.Entities.Client;
import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import domain.Validators.*;
import javafx.util.Pair;
import ro.ubb.socket.common.Message;
import ro.ubb.socket.common.ServiceInterface.ReportsInterface;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.RepositoryDB.BookRepositoryDB;
import ro.ubb.socket.server.repository.RepositoryDB.ClientRepositoryDB;
import ro.ubb.socket.server.repository.RepositoryDB.PurchaseRepositoryDB;
import ro.ubb.socket.server.repository.SortingRepository;
import ro.ubb.socket.server.service.*;
import ro.ubb.socket.server.service.Service.BookService;
import ro.ubb.socket.server.service.Service.ClientService;
import ro.ubb.socket.server.service.Service.PurchaseService;
import ro.ubb.socket.server.service.ServiceServer.BookServiceServer;
import ro.ubb.socket.server.service.ServiceServer.ClientServiceServer;
import ro.ubb.socket.server.service.ServiceServer.PurchaseServiceServer;
import ro.ubb.socket.server.tcp.TcpServer;
import ro.ubb.socket.common.ServiceInterface.bookServiceInterface;
import ro.ubb.socket.common.ServiceInterface.clientServiceInterface;
import ro.ubb.socket.common.ServiceInterface.purchaseServiceInterface;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerApp {


    public static void main(String[] args) {
        try {
            String ERROR = "error";
            String OK = "ok";

            Validator<Book> bookValidator = new BookValidator();
            Validator<Client> clientValidator = new ClientValidator();
            Validator<Long> idValidator = new IdValidator();
            Validator<Purchase> purchaseValidator = new PurchaseValidator();

            SortingRepository<Long, Book> repobook = new BookRepositoryDB(bookValidator, idValidator);
            SortingRepository<Long, Client> repoclient = new ClientRepositoryDB(clientValidator, idValidator);
            SortingRepository<Long, Purchase> repopurchase = new PurchaseRepositoryDB(purchaseValidator, idValidator);

            BookService bookService = new BookService(repobook);
            ClientService clientService = new ClientService(repoclient);
            PurchaseService purchaseService = new PurchaseService(repopurchase);

            System.out.println("server started");
            ExecutorService executorService = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors()
            );

            BookServiceServer bookServiceServer = new BookServiceServer(bookService, purchaseService, executorService);
            ClientServiceServer clientServiceServer = new ClientServiceServer(clientService, purchaseService, executorService);
            PurchaseServiceServer purchaseServiceServer = new PurchaseServiceServer(clientService, bookService, purchaseService, executorService);
            Reports reports = new Reports(clientService, bookService, purchaseService, executorService);

            TcpServer tcpServer = new TcpServer(executorService);

            ///CLIENT PART
            tcpServer.addHandler(clientServiceInterface.ADD_CLIENT, (request) -> {

                Client x = (Client) request.getBody();
                try {
                    String y = clientServiceServer.addClient(x).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(clientServiceInterface.UPDATE_CLIENT, (request) -> {
                Client x = (Client) request.getBody();
                try {
                    String y = clientServiceServer.updateClient(x).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(clientServiceInterface.DELETE_CLIENT, (request) -> {
                Long id = (Long) request.getBody();
                try {
                    String y = clientServiceServer.deleteClient(id).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(clientServiceInterface.GET_CLIENT, (request) -> {
                Long id = (Long) request.getBody();
                try {
                    Client y = clientServiceServer.getClient(id).get();
                    Long id1 = y.getId();
                    if (id1 == -1L) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(clientServiceInterface.PRINT_ALL_CLIENT, (request) -> {

                try {
                    Set<Client> x = clientServiceServer.getAllClient().get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(clientServiceInterface.GET_SORTED_CLIENTS, (request) -> {
                List<Client> x;
                Sort sort = (Sort) request.getBody();
                try {
                    x = clientServiceServer.getSortedClients(sort).get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });


            //BOOK PART

            tcpServer.addHandler(bookServiceInterface.ADD_BOOK, (request) -> {
                Book book = (Book) request.getBody();
                try {
                    String y = bookServiceServer.addBook(book).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(bookServiceInterface.UPDATE_BOOK, (request) -> {

                Book book = (Book) request.getBody();
                try {
                    String y = bookServiceServer.updateBook(book).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(bookServiceInterface.DELETE_BOOK, (request) -> {
                Long id = (Long) request.getBody();
                try {
                    String y = bookServiceServer.deleteBook(id).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(bookServiceInterface.GET_BOOK, (request) -> {
                Long id = (Long) request.getBody();
                try {
                    Book y = bookServiceServer.getBook(id).get();
                    Long id1 = y.getId();
                    if (id1 == -1L) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(bookServiceInterface.PRINT_ALL_BOOK, (request) -> {
                try {
                    Set<Book> x = bookServiceServer.getAllBook().get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }

            });

            tcpServer.addHandler(bookServiceInterface.GET_SORTED_BOOKS, (request) -> {
                Sort sort = (Sort) request.getBody();
                List<Book> x;
                try {
                    x = bookServiceServer.getSortedBooks(sort).get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            //PURCHASE PART

            tcpServer.addHandler(purchaseServiceInterface.BUY_BOOK, (request) -> {
                Purchase body = (Purchase) request.getBody();
                try {
                    String y = purchaseServiceServer.buy(body).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(purchaseServiceInterface.PRINT_ALL_PURCHASE, (request) -> {

                try {
                    Set<Purchase> x = purchaseServiceServer.getAllPurchases().get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }


            });

            tcpServer.addHandler(purchaseServiceInterface.GET_PURCHASE, (request) -> {

                Pair<Long, Long> body = (Pair<Long, Long>) request.getBody();
                Long idClient = body.getKey();
                Long idBook = body.getValue();

                try {
                    Purchase x = purchaseServiceServer.getPurchase(idClient, idBook).get();
                    if (x.getIdBook() == -1 || x.getIdClient() == -1) return new Message(ERROR, x);
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {

                    return new Message(ERROR, e.getMessage());
                }

            });

            tcpServer.addHandler(purchaseServiceInterface.GET_SORTED_PURCHASES, (request) -> {
                List<Purchase> x = purchaseService.getPurchasesSorted((Sort) request.getBody());
                return new Message(OK, x);
            });


            tcpServer.addHandler(purchaseServiceInterface.CHANGE_DATE, (request) -> {
                Pair<Long, Pair<Long, Date>> body = (Pair<Long, Pair<Long, Date>>) request.getBody();
                Long idClient = body.getKey();
                Long idBook = body.getValue().getKey();
                Date date = body.getValue().getValue();
                try {
                    String y = purchaseServiceServer.changeDate(idClient, idBook, date).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);

                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.toString());
                }
            });

            tcpServer.addHandler(purchaseServiceInterface.RETURN_BOOK, (request) -> {
                Pair<Long,Long> body = (Pair<Long,Long>) request.getBody();
                Long idClient = body.getKey();
                Long idBook = body.getValue();
                try {
                    String y = purchaseServiceServer.reTurn(idClient, idBook).get();
                    if (y.contains("ERROR")) return new Message(ERROR, y);
                    return new Message(OK, y);

                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.toString());
                }

            });

            //REPORTS
            tcpServer.addHandler(ReportsInterface.FILTER_BOOKS_BY_TITLE, (request) -> {

                String arg = (String) request.getBody();
                try {
                    Set<Book> x = reports.filterBooksByTitles(arg).get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(ReportsInterface.FILTER_CLIENTS_BY_NAME, (request) -> {

                String arg = (String) request.getBody();
                try {
                    Set<Client> result = reports.filterClientsByName(arg).get();
                    return new Message(OK, result);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(ReportsInterface.FILTER_CLIENTS_WITH_MORE_BOOKS, (request) -> {
                int x = (int) request.getBody();
                try {
                    Set<Client> result = reports.filterClientsWithMoreThanXBooks(x).get();
                    return new Message(OK, result);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(ReportsInterface.FILTER_PURCHASES_FROM_book_X, (request) -> {
                Long x = (Long) request.getBody();
                try {
                    Set<Purchase> result = reports.getAllPurchaseofBookX(x).get();
                    return new Message(OK, result);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });
            tcpServer.addHandler(ReportsInterface.FILTER_PURCHASES_FROM_CLIENT_X, (request) -> {

                Long x = (Long) request.getBody();
                try {
                    Set<Purchase> result = reports.getAllPurchaseFromClientX(x).get();
                    return new Message(OK, result);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(ReportsInterface.FILTER_PURCHASES_BY_DATE_BEFORE, (request) -> {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date x = (Date) request.getBody();
                try {
                    Set<Purchase> result = reports.filterPurchasesByDateBefore(x).get();
                    return new Message(OK, result);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(ReportsInterface.REMOVE_BOOK_IF_PRICE_LESS_THAN_S, (request) -> {

                int x = (int) request.getBody();
                try {
                    Set<Book> result = reports.removeBookIfPriceLessThenS(x).get();
                    return new Message(OK, result);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.addHandler(ReportsInterface.SORT_CLIENTS_BY_MONEY, (request) -> {
                try {
                    List<Client> x = reports.sortClientsByMoneySpent().get();
                    return new Message(OK, x);
                } catch (InterruptedException | ExecutionException e) {
                    return new Message(ERROR, e.getMessage());
                }
            });

            tcpServer.startServer();
            executorService.shutdown();
        } catch (RuntimeException | SQLException | ValidatorException ex) {
            ex.printStackTrace();
        }

    }
}
