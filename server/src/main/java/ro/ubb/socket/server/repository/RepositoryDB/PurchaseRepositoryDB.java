package ro.ubb.socket.server.repository.RepositoryDB;



import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.SortingRepository;

import java.sql.Date;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PurchaseRepositoryDB implements SortingRepository<Long, Purchase> {

    private Validator<Purchase> purchaseValidator;
    private Validator<Long> keyValidator;
    private HashMap<Long, Purchase> entities;
    private static final String URL = "jdbc:postgresql://localhost:5432/BookStore";
    private static final String USER = "postgres";
    private static final String PASSWORD = "123";


    public PurchaseRepositoryDB(Validator<Purchase> validator, Validator<Long> keyValidator) throws SQLException, ValidatorException {
        this.keyValidator = keyValidator;
        this.purchaseValidator = validator;
        entities = new HashMap<>();
        loadData();
    }
    private void loadData() throws ValidatorException, SQLException {
        Connection con = null;
        con = DriverManager.getConnection(URL, USER, PASSWORD);
        String sql = "Select idclient, idbook, datepurchase,id from purchase";
        PreparedStatement stmt = con.prepareStatement(sql);
        ResultSet set = stmt.executeQuery();
        while (set.next()) {
            Long idClient = set.getLong("idclient");
            Long idBook = set.getLong("idbook");
            Date date = set.getDate("datepurchase");
            Purchase p = new Purchase(idClient, idBook,date);
            purchaseValidator.validate(p);
            entities.put(p.getId(), p);
        }
        con.close();
    }

    @Override
    public Iterable<Purchase> findAll(Sort sort) {
        List<Purchase> all = new ArrayList<>(entities.values());
        all.sort(sort);
        return all;
    }

    @Override
    public Optional<Purchase> findOne(Long id) {
        if (id == null){
            throw new IllegalArgumentException("null id");
        }
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public Iterable<Purchase> findAll() {
        return entities.entrySet().stream()
                .map(Map.Entry::getValue).collect(Collectors.toSet());
    }

    private void saveToDb(Purchase entity){
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


            String sql = "insert into purchase( idclient, idbook,datepurchase,id) values(?,?,?,?)";
            String x = dateFormat.format(entity.getDate());
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, entity.getIdClient());
            stmt.setLong(2, entity.getIdBook());
            stmt.setDate(3, Date.valueOf(x));
            stmt.setLong(4, entity.getId());
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Purchase> save(Purchase entity) {
        if (entity.getId() == null){
            throw new IllegalArgumentException("null id");
        }
        if (!entities.containsKey(entity.getId())){
            saveToDb(entity);
        }
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    private void deleteFromDb(Long id){
        Connection con = null;
        try{

            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "delete from purchase where id=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, id);
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Purchase> delete(Long id) {
        if (id == null){
            throw new IllegalArgumentException("null id");
        }
        if (entities.containsKey(id)){
            deleteFromDb(id);
        }
        return Optional.ofNullable(entities.remove(id));
    }

    private void updateToDb(Purchase entity){
        Connection con = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String x = dateFormat.format(entity.getDate());

            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "update purchase set datepurchase=? where idClient=? and idBook=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setDate(1, Date.valueOf(x));
            stmt.setLong(2, entity.getIdClient());
            stmt.setLong(3, entity.getIdBook());
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Purchase> update(Purchase entity) {
        if (entity == null){
            throw new IllegalArgumentException("null entity");
        }
        if (entities.containsKey(entity.getId())){
            updateToDb(entity);
        }
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k, v) -> entity));
    }
}
