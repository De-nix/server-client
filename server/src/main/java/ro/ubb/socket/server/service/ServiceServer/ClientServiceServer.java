package ro.ubb.socket.server.service.ServiceServer;
import domain.Entities.Client;
import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.common.ServiceInterface.clientServiceInterface;
import ro.ubb.socket.server.service.Service.ClientService;
import ro.ubb.socket.server.service.Service.PurchaseService;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class ClientServiceServer implements clientServiceInterface {
    ClientService clientService;
    PurchaseService purchaseService;
    private ExecutorService executorService;

    public ClientServiceServer(ClientService clientService, PurchaseService purchaseService, ExecutorService executorService) {
        this.clientService = clientService;
        this.purchaseService = purchaseService;
        this.executorService = executorService;
    }

    /**
     * @param client - the client that will be added
     */
    public synchronized CompletableFuture<String> addClient(Client client)  {
        return CompletableFuture.supplyAsync(() ->
        {
            try {
                clientService.getClient(client.getId());
                return "ERROR : The client already exists";
            } catch (ValidatorException ignore){
            }
            try{
                clientService.addClient(client);
                return "The client was successfully added\n";
            } catch (ValidatorException e) {
                return "ERROR : "+e.toString()+"\n";
            }
        });
    }

    /**
     * @param id - the client that will be deleted
     */
    public synchronized CompletableFuture<String> deleteClient(Long id)  {

        return CompletableFuture.supplyAsync(() ->
        {
            try {
                clientService.getClient(id);

                purchaseService.getAllPurchases().stream().filter(x -> x.getIdClient().equals(id)).map(Purchase::getIdClient).forEach(x -> {
                    try {
                        purchaseService.deletePurchase(id, x);
                    } catch (ValidatorException | IOException ignored) {
                    }
                });
                clientService.deleteClient(id);
                return "The client was successfully deleted\n";
            } catch (ValidatorException | IOException e) {
                return "ERROR : "+e.toString()+"\n";
            }
        });

    }

    /**
     * @param client - the updated client with the same id as the old one

     */
    public synchronized CompletableFuture<String> updateClient(Client client) {

        return CompletableFuture.supplyAsync(() ->
        {
            try {
                clientService.getClient(client.getId());
                clientService.updateClient(client);
                return "The client was successfully updated\n";
            } catch (ValidatorException | IOException e) {
                return "ERROR : "+e.toString()+"\n";
            }
        });
    }

    /**
     * @param id - the id of a client
     * @return - the client with the id = 'id'
     */
    public synchronized CompletableFuture<Client> getClient(Long id) {
        return CompletableFuture.supplyAsync(() ->
        {
            try {
                return clientService.getClient(id);
            } catch (ValidatorException e) {
                Client c = new Client(e.toString());
                c.setId(-1L);
                return c;
            }
        });
    }

    /**
     * @return - a set of all the clients
     */

    @Override
    public synchronized CompletableFuture<Set<Client>> getAllClient() {
        return CompletableFuture.supplyAsync(() ->
                clientService.getAllClients(),executorService);
    }
    public synchronized CompletableFuture<List<Client>> getSortedClients(Sort sort) {
        return CompletableFuture.supplyAsync(() ->
                clientService.getClientsSortedByName(sort),executorService);
    }

}
