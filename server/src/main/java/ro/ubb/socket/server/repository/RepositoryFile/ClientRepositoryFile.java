package ro.ubb.socket.server.repository.RepositoryFile;


import domain.Entities.Client;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.server.repository.InMemoryRepository;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ClientRepositoryFile extends InMemoryRepository<Long, Client> {
    private final String fileName;

    public ClientRepositoryFile(Validator<Client> validator, Validator<Long> keyValidator, String fileN) {
        super(validator, keyValidator);
        this.fileName = fileN;
        loadData();
    }

    private void loadData() {
        Path path = Paths.get(fileName);
        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));
                Long id = Long.valueOf(items.get(0));
                String name = items.get(1);
                Client client = new Client(name);
                client.setId(id);
                try {
                    super.save(client);
                } catch (ValidatorException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        Optional<Client> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Client> update(Client entity) throws ValidatorException, IOException {
        Optional<Client> optional = super.update(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveAllToFile();
        return Optional.empty();
    }

    @Override
    public Optional<Client> delete(Long id) throws ValidatorException, IOException {
        Optional<Client> optional = super.delete(id);
        if (!optional.isPresent()) {
            return optional;
        }
        saveAllToFile();
        return Optional.empty();
    }

    private void saveToFile(Client entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(entity.getId() + "," + entity.getName());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void saveAllToFile() throws IOException {
        Path path = Paths.get(fileName);

        BufferedWriter bufferedWriter = Files.newBufferedWriter(path) ;
        this.findAll().forEach(entity->
        {
            try {
                bufferedWriter.write(entity.getId() + "," + entity.getName());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
