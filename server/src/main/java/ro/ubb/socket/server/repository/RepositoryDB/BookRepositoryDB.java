package ro.ubb.socket.server.repository.RepositoryDB;

import domain.Entities.Book;
import domain.Exceptions.ValidatorException;
import domain.Validators.Validator;
import ro.ubb.socket.common.Sort;
import ro.ubb.socket.server.repository.SortingRepository;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class BookRepositoryDB implements SortingRepository<Long, Book > {

    private HashMap<Long, Book> entities;
    private static final String URL = "jdbc:postgresql://localhost:5432/BookStore";
    private static final String USER = "postgres";
    private static final String PASSWORD = "123";
    private Validator<Book> bookValidator;
    private Validator<Long> keyValidator;


    public BookRepositoryDB(Validator<Book> validator, Validator<Long> keyValidator) throws SQLException {
        this.keyValidator = keyValidator;
        this.bookValidator = validator;
        entities = new HashMap<>();
        loadData();
    }

    private void loadData() throws SQLException {
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "Select * from book";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet set = stmt.executeQuery();
            while (set.next()) {
                Long id = set.getLong("id");
                String title = set.getString("title");
                String author = set.getString("author");
                int price = set.getInt("price");
                Book b = new Book(title,author,price);
                b.setId(id);
                entities.put(id, b);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Iterable<Book> findAll(Sort sort) {
       List<Book> all = new ArrayList<>(entities.values());
        //all.stream().forEach(student -> System.out.println(student));
        all.sort(sort);
        return all;
    }


    @Override
    public Optional<Book> findOne(Long id) {
        if (id == null){
            throw new IllegalArgumentException("null id");
        }
        return Optional.ofNullable(entities.get(id));
    }

    @Override
    public Iterable<Book> findAll() {
        Set<Book> all = entities.entrySet().stream()
                .map(entry -> entry.getValue()).collect(Collectors.toSet());
        return all;
    }

    private void saveToDb(Book entity) throws ValidatorException {

        Connection con = null;
        this.keyValidator.validate(entity.getId());
        this.bookValidator.validate(entity);

        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "insert into book(id, title, author, price) values(?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, entity.getId());
            stmt.setString(2, entity.getTitle());
            stmt.setString(3, entity.getAuthor());
            stmt.setInt(4,entity.getPrice());
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Book> save(Book entity) throws ValidatorException {

        this.keyValidator.validate(entity.getId());
        this.bookValidator.validate(entity);
        if (entity.getId() == null){
            throw new IllegalArgumentException("null id");
        }

        Optional<Book> o  = Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));

        saveToDb(entity);
        return o;
        }

    private void deleteFromDb(Long id) throws ValidatorException {

        this.keyValidator.validate(id);
        Connection con = null;
        try{
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "delete from book where id=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setLong(1, id);
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Book> delete(Long id) throws ValidatorException {
        this.keyValidator.validate(id);
        if (id == null){
            throw new IllegalArgumentException("null id");
        }
        if (entities.containsKey(id)){
            deleteFromDb(id);
        }
        return Optional.ofNullable(entities.remove(id));
    }

    private void updateToDb(Book entity) throws ValidatorException {

        this.keyValidator.validate(entity.getId());
        this.bookValidator.validate(entity);

        Connection con = null;
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            String sql = "update book set title=?, author=?, price=? where id=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, entity.getTitle());
            stmt.setString(2, entity.getAuthor());
            stmt.setInt(3, entity.getPrice());
            stmt.setLong(4, entity.getId());
            stmt.execute();

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Book> update(Book entity) throws ValidatorException {

        this.keyValidator.validate(entity.getId());
        this.bookValidator.validate(entity);
        if (entity == null){
            throw new IllegalArgumentException("null entity");
        }
        if (entities.containsKey(entity.getId())){
            updateToDb(entity);
        }
        return Optional.ofNullable(entities.computeIfPresent(entity.getId(), (k,v) -> entity));
    }
}
