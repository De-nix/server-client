package ro.ubb.socket.common;

import java.io.*;

public class Message {
    public static final int PORT = 1234;
    public static final String HOST = "localhost";

    private String header;
    private Object body;

    public Message() {
    }

    public Message(String header, Object body) {
        this.header = header;
        this.body = body;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public void writeTo(OutputStream os) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(header);
        oos.writeObject(body);
    }

    public void readFrom(InputStream is) throws IOException, ClassNotFoundException {
        ObjectInputStream br = new ObjectInputStream(is);
        header = (String)br.readObject();
        body = br.readObject();
    }

    @Override
    public String toString() {
        return "Message{" +
                "header='" + header + '\n' +
                ", body='" + body + '\n' +
                '}';
    }
}
