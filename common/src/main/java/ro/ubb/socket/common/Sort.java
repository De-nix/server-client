package ro.ubb.socket.common;

import domain.Exceptions.ValidatorException;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Sort implements Comparator<Object> , Serializable {
    private List<String> fields;
    private List<Boolean> direction;

    public Sort(List<String> fields, List<Boolean> direction) {
        this.fields = fields;
        this.direction = direction;
    }

    public enum Dir {
        ascending(true),
        descending(false);
        final boolean value;

        Dir(boolean val) {
            this.value = val;
        }
    }

    public Sort(Dir dir, String... myFields) {

        this.fields = new ArrayList<>();
        this.direction = new ArrayList<>();
        this.fields.addAll(Arrays.asList(myFields));
        for (int i = 0; i < myFields.length; i++) this.direction.add(dir.value);
    }

    public Sort(String... myFields) {
        this.fields = new ArrayList<>();
        this.direction = new ArrayList<>();
        this.fields.addAll(Arrays.asList(myFields));
        for (int i = 0; i < myFields.length; i++) this.direction.add(true);
    }

    public Sort add(Sort sort) {

        this.fields.addAll(sort.fields);
        this.direction.addAll(sort.direction);
        return this;
    }

    @Override
    public int compare(Object o1, Object o2) {
        int res = 0;
        if (!o1.getClass().equals(o2.getClass()))
            try {
                throw new ValidatorException("Objects that need to be sorted are from different classes\n");
            } catch (ValidatorException e) {
                e.printStackTrace();
                return 0;
            }
        try {
            return cmpObj(o1, o2);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return res;
    }


    private int cmpObj(Object o1, Object o2) throws NoSuchFieldException, IllegalAccessException {

        int i = 0;
        while (i < fields.size()) {
            Field f = null;
            if (fields.get(i).equals("id"))
                f = o1.getClass().getSuperclass().getDeclaredField(fields.get(i));
            else
                f = o1.getClass().getDeclaredField(fields.get(i));
            boolean oldStatus;
            oldStatus = f.isAccessible();
            f.setAccessible(true);
            int x = 0;
            if (direction.get(i)) x = ((Comparable) f.get(o1)).compareTo(f.get(o2));
            else x = -((Comparable) f.get(o1)).compareTo(f.get(o2));

            f.setAccessible(oldStatus);
            i += 1;
            if (x != 0) return x;
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder x = new StringBuilder();
        for (int i = 0; i < this.direction.size(); i++) {
            x.append(fields.get(i));
            x.append(",");
            x.append(direction.get(i));
            x.append("]]]");
        }
        return x.toString();
    }

    public static Sort readSort(String x) {
        List<String> lines = Arrays.asList(x.split("]]]"));
        List<String> fds = new ArrayList<>();
        List<Boolean> dirs = new ArrayList<>();

        lines.forEach(
                line -> {
                    List<String> words = Arrays.asList(line.split(","));
                    if (words.size() > 0) {
                        fds.add(words.get(0));
                        dirs.add(Boolean.valueOf(words.get(1)));
                    }
                }
        );
        Sort s = new Sort();
        s.fields = fds;
        s.direction = dirs;
        return s;
    }
}
