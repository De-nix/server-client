package ro.ubb.socket.common.ServiceInterface;

import domain.Entities.Book;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface bookServiceInterface {
    String ADD_BOOK = "addBook";
    String DELETE_BOOK = "deleteBook";
    String UPDATE_BOOK = "updateBook";
    String PRINT_ALL_BOOK = "getAllBook";
    String GET_BOOK = "getBook";
    String GET_SORTED_BOOKS = "getSortedBooks";

    CompletableFuture<String> addBook(Book book) throws ValidatorException;
    /**
     * @param id - the id of the book that will be deleted
     * @throws ValidatorException - an error if the id is not valid
     */
    CompletableFuture<String> deleteBook(Long id) throws ValidatorException, IOException;

    /**
     * @param book - the updated book with the same id as the old one
     * @throws ValidatorException - if the book parameter was invalid
     */
    CompletableFuture<String> updateBook(Book book) throws ValidatorException, IOException ;

    /**
     * @param id - id of the book
     * @return - the book with id = 'id'
     * @throws ValidatorException - if the book with id = 'id' doesn't exist
     */
    CompletableFuture<Book> getBook(Long id) throws ValidatorException;

    CompletableFuture<List<Book>> getSortedBooks(Sort sort);

    CompletableFuture<Set<Book>> getAllBook();

}
