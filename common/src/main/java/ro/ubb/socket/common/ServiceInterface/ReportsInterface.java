package ro.ubb.socket.common.ServiceInterface;

import domain.Entities.Book;
import domain.Entities.Client;
import domain.Entities.Purchase;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
public interface ReportsInterface {


    String SORT_CLIENTS_BY_MONEY ="sortClientsByMoneySpent";
    String FILTER_BOOKS_BY_TITLE="filterBooksByTitles";
    String REMOVE_BOOK_IF_PRICE_LESS_THAN_S = "removeBookIfPriceLessThenS";
    String FILTER_CLIENTS_BY_NAME = "filterClientsByName";
    String FILTER_PURCHASES_BY_DATE_BEFORE = "filterPurchasesByDateBefore";
    String FILTER_PURCHASES_FROM_CLIENT_X = "getAllPurchaseFromClientX";
    String FILTER_PURCHASES_FROM_book_X = "getAllPurchaseofBookX";
    String FILTER_CLIENTS_WITH_MORE_BOOKS = "filterClientsWithMoreThanXBooks";

    CompletableFuture<Set<Client>> filterClientsByName(String s);
    CompletableFuture<Set<Purchase>> filterPurchasesByDateBefore(Date date);
    CompletableFuture<Set<Purchase>> getAllPurchaseFromClientX(Long idClient);

    CompletableFuture<Set<Client>> filterClientsWithMoreThanXBooks(int no);
    CompletableFuture<Set<Purchase>> getAllPurchaseofBookX(Long idBook);
    CompletableFuture<List<Client>> sortClientsByMoneySpent();

    /**
     * Returns all Books whose name contain the given string.
     *
     * @param s - the given string
     * @return - set of books that contains the given string
     */
    CompletableFuture<Set<Book>> filterBooksByTitles(String s) ;

    /**
     *
     * @param s - the given price
     * @return - a set of books that have the price greater than s
     */

    CompletableFuture<Set<Book>> removeBookIfPriceLessThenS(int s) ;
    /**
     * @param s - a given string
     * @return - the set of clients whose names contain that string
     */



}