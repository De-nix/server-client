package ro.ubb.socket.common.ServiceInterface;

import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface purchaseServiceInterface {


    String PRINT_ALL_PURCHASE = "getAllPurchases";
    String BUY_BOOK = "buy";
    String CHANGE_DATE = "changeDate";
    String GET_SORTED_PURCHASES = "getSortedPurchases";
    String GET_PURCHASE = "getPurchase";
    String RETURN_BOOK = "reTurn";
    String SPENT_MONEY = "spentMoney";

    CompletableFuture<Set<Purchase>> getAllPurchases();
    CompletableFuture<List<Purchase>> getSortedPurchases(Sort sort);
    CompletableFuture<String> buy(Purchase entity) throws ValidatorException;
    CompletableFuture<String> reTurn(Long clientId, Long bookId) throws ValidatorException, IOException;
    CompletableFuture<String> changeDate(Long clientId, Long bookId, Date date) throws IOException, ValidatorException ;
    CompletableFuture<Purchase> getPurchase(Long clientId, Long bookId) throws ValidatorException;


}
