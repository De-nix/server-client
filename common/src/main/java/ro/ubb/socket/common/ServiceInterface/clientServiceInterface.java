package ro.ubb.socket.common.ServiceInterface;

import domain.Entities.Client;
import domain.Exceptions.ValidatorException;
import ro.ubb.socket.common.Sort;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public interface clientServiceInterface {

    String ADD_CLIENT = "addClient";
    String DELETE_CLIENT = "deleteClient";
    String UPDATE_CLIENT = "updateClient";
    String PRINT_ALL_CLIENT = "getAllClients";
    String GET_CLIENT = "getClient";
    String GET_SORTED_CLIENTS = "getSortedClients";


    CompletableFuture<String> addClient(Client client) throws ValidatorException;
    CompletableFuture<Set<Client>> getAllClient();
    /**
     * @param id - the client that will be deleted
     * @throws ValidatorException - if the client is not valid
     */
    CompletableFuture<String> deleteClient(Long id) throws ValidatorException, IOException;
    /**
     * @param client - the updated client with the same id as the old one
     * @throws ValidatorException - if the new client is not valid
     */
    CompletableFuture<String> updateClient(Client client) throws ValidatorException, IOException;
    /**
     * @param id - the id of a client
     * @return - the client with the id = 'id'
     * @throws ValidatorException - if the client doesn't exist
     */
    CompletableFuture<Client> getClient(Long id) throws ValidatorException ;

    CompletableFuture<List<Client>> getSortedClients(Sort sort);


}
