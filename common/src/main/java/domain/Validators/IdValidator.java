package domain.Validators;

import domain.Exceptions.ValidatorException;

public class IdValidator implements Validator<Long> {
    @Override
    public void validate(Long entity) throws ValidatorException {
        if(entity == null) throw new ValidatorException("The Id should not be null\n");
        if(entity<=0) throw new ValidatorException("The Id should be greater than 0\n");
    }
}
