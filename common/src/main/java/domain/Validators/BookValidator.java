package domain.Validators;


import domain.Entities.Book;
import domain.Exceptions.ValidatorException;

public class BookValidator implements Validator<Book> {
    @Override
    public void validate(Book entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no book. Sorry.\n");
        if (entity.getAuthor().equals("")) throw new ValidatorException("There is no author for this book. Sorry.\n");
        if (entity.getTitle().equals("")) throw new ValidatorException("OOPS! You forgot the title!!\n");
        if (entity.getPrice()<=0) throw new ValidatorException("The Price of the book should be greater than 0\n");

    }
}
