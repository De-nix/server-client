package domain.Validators;

import domain.Exceptions.ValidatorException;
import domain.Entities.Client;

public class ClientValidator implements Validator<Client> {
    @Override
    public void validate(Client entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no client. Sorry.\n");
        if (entity.getName().equals("")) throw new ValidatorException("There is no name\n");
        if (entity.getId()<=0) throw new ValidatorException("OOPS! Id issue!!\n");
    }
}
