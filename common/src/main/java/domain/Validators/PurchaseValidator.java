package domain.Validators;


import domain.Entities.Purchase;
import domain.Exceptions.ValidatorException;

public class PurchaseValidator implements Validator<Purchase> {
    @Override
    public void validate(Purchase entity) throws ValidatorException {
        if (entity==null) throw new ValidatorException("There is no purchase. Sorry.\n");
        if (entity.getDate() == null) throw new ValidatorException("There is no date\n");
        if (entity.getIdBook()<=0) throw new ValidatorException("OOPS! BookId is invalid!!\n");
        if (entity.getIdClient()<=0) throw new ValidatorException("OOPS! ClientId is invalid!!\n");
    }}


