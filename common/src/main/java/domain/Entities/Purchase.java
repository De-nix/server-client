package domain.Entities;

import domain.Exceptions.RepositoryException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class Purchase extends BaseEntity<Long> {
    private Long idClient;
    private Long idBook;
    private Date date;

    static private AtomicLong id = new AtomicLong(1);
    public Purchase(Long idClient, Long idBook, Date date) {
        this.idClient = idClient;
        this.idBook = idBook;
        this.date = date;
        this.setId(Purchase.id.getAndIncrement());
    }

    public static AtomicLong getIdAtomic() {
        return id;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        
        return this.getIdClient()+","+ this.getIdBook()+","+formatter.format(this.getDate());
    }
    public static Purchase readPurchase(String line){

        List<String> items = Arrays.asList(line.split(","));
        Long idClient = Long.valueOf(items.get(0));
        Long idBook = Long.valueOf(items.get(1));
        Date date = null;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(items.get(2));
        } catch (ParseException ign) {
            throw new RepositoryException("Invalid data load!\n");
        }
        return new Purchase(idClient,idBook,date);

    }
}
