package domain.Entities;

import java.util.Arrays;
import java.util.List;

public class Book extends BaseEntity<Long>{

   private String title;
   private String author;
   private int price;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    public Book(String title, String author, int price) {
        this.title = title;
        this.author = author;
        this.price = price;
    }


    public String getTitle(){return this.title;}
    public String getAuthor(){return this.author;}
    public int getPrice(){return this.price;}

    @Override
    public String toString() {
        //return "book{" +
        //        "title='" + title + '\'' +
        //        ", author='" + author + '\'' +
        //        ", price=" + price +
        //        '}';
        return this.getId()+","+this.getTitle()+","+this.getAuthor()+","+this.getPrice();
    }

    public static Book readBook(String input){
        List<String> items = Arrays.asList(input.split(","));
        Long id = Long.valueOf(items.get(0));
        String title = items.get(1);
        String author = items.get(2);
        int price = Integer.parseInt(items.get(3));
        Book book = new Book(title,author,price);
        book.setId(id);
        return book;
    }
}
