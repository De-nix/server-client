package domain.Entities;

import java.util.Arrays;
import java.util.List;

public class Client extends BaseEntity<Long>  {
    private String name;

    public Client(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return this.getId()+","+this.getName();
    }

    public static Client readClient(String input){
        List<String> items = Arrays.asList(input.split(","));
        Long id = Long.valueOf(items.get(0));
        String name = items.get(1);
        Client client = new Client(name);
        client.setId(id);
        return client;
    }

}
